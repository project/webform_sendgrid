Webform SendGrid

https://www.drupal.org/project/webform_sendgrid

# Configuration

 1. Configure SendGrid API. See the README.
 2. Add/edit an existing Webform.
    Your Webform should have an _email_ field. Add an email field if one does
    not yet exist.
 3. Navigate to _Settings -> Emails/Handlers_ tab.
 4. Click _Add Handler_ button.
 5. Click _SendGrid_ option.
 6. Select an existing list from the Dropdown. Create a new list in the
    SendGrid UI if one does not exist.
 7. Select the email field.
 8. Optionally, add extra Fields or Custom fields. See the field Help for more
    information on how mappings work.
 9. Save the handler.

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
