<?php

declare(strict_types = 1);

namespace Drupal\webform_sendgrid\Plugin\WebformHandler;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends Webform submission data to SendGrid.
 *
 * @WebformHandler(
 *   id = "sendgrid",
 *   label = @Translation("SendGrid"),
 *   category = @Translation("SendGrid"),
 *   description = @Translation("Sends a form submission to a SendGrid list."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformSendGridHandler extends WebformHandlerBase {

  /**
   * A SendGrid client instance.
   *
   * @var \SendGrid
   */
  protected $sendGrid;

  /**
   * The Webform token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $webformTokenManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->sendGrid = $container->get('sendgrid_api.client');
    $instance->webformTokenManager = $container->get('webform.token_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list' => '',
      'email_field' => '',
      'reserved_fields' => [],
      'custom_fields' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['sendgrid'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SendGrid settings'),
    ];

    // List.
    $listOptions = [];
    foreach ($this->getLists() as ['id' => $id, 'name' => $name]) {
      $listOptions[$id] = $name;
    }

    $form['sendgrid']['list'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('List'),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $this->configuration['list'],
      '#options' => $listOptions,
      '#description' => $this->t('Select the list you want to send this submission to. Alternatively, you can also use the Other field for token replacement.'),
    ];

    // Email field.
    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $emailFieldOptions = [];
    foreach ($fields as $fieldMachineName => $fieldInfo) {
      if (in_array($fieldInfo['#type'], ['email', 'webform_email_confirm'], TRUE)) {
        $emailFieldOptions[$fieldMachineName] = $fieldInfo['#title'];
      }
    }

    $form['sendgrid']['email_field'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Email field'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['email_field'],
      '#options' => $emailFieldOptions,
      '#empty_option' => $this->t('- Select -'),
      '#description' => $this->t('Select which field to get the email address from. Create an email field if no options are available. Alternatively, you can also use the Other field for token replacement.'),
    ];

    ['reserved' => $reserved, 'custom' => $custom] = $this->getFieldDefinitions();

    // Reserved fields.
    $form['sendgrid']['reserved_fields'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Fields'),
      '#default_value' => $this->configuration['reserved_fields'],
      '#description' => $this->t("You can map additional fields from your Webform to fields in your list. Add one per line. For example: <code>first_name: '[webform_submission:values:first_name]'</code>. You may use tokens."),
    ];

    $form['sendgrid']['reserved_fields_list'] = [
      '#type' => 'table',
      '#header' => [
        'name' => $this->t('ID'),
        'type' => $this->t('Type'),
      ],
      '#rows' => array_map(function (array $result): array {
        $row = [];
        $row['name']['data'] = [
          '#type' => 'inline_template',
          '#template' => '<code>{{ name }}</code>',
          '#context' => [
            'name' => $result['name'],
          ],
        ];
        $row['type']['data']['#plain_text'] = $result['field_type'];
        return $row;
      }, $reserved),
      '#empty' => $this->t('Error.'),
    ];

    // Custom fields.
    $form['sendgrid']['custom_fields'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Custom fields'),
      '#default_value' => $this->configuration['custom_fields'],
      '#description' => $this->t("You can map additional fields from your Webform to custom fields in your list. Add one per line. For example: <code>CUSTOM_FIELD_ID: '[webform_submission:values:my_field]'</code>. The name here is not the <em>Field Name</em> as exposed in the SendGrid UI. See below for valid IDs for this SendGrid account. You may use tokens."),
    ];

    $form['sendgrid']['custom_fields_list'] = [
      '#type' => 'table',
      '#header' => [
        'id' => $this->t('CUSTOM_FIELD_ID'),
        'name' => $this->t('Name'),
        'type' => $this->t('Type'),
      ],
      '#rows' => array_map(function (array $result): array {
        $row = [];
        $row['id']['data'] = [
          '#type' => 'inline_template',
          '#template' => '<code>{{ id }}</code>',
          '#context' => [
            'id' => $result['id'],
          ],
        ];
        $row['name']['data']['#plain_text'] = $result['name'];
        $row['type']['data']['#plain_text'] = $result['field_type'];
        return $row;
      }, $custom),
      '#empty' => $this->t('No custom fields found.'),
    ];

    $form['sendgrid']['token_tree_link'] = $this->webformTokenManager->buildTreeLink();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['list'] = $form_state->getValue(['sendgrid', 'list']);
    $this->configuration['email_field'] = $form_state->getValue(['sendgrid', 'email_field']);
    $this->configuration['reserved_fields'] = $form_state->getValue(['sendgrid', 'reserved_fields']);
    $this->configuration['custom_fields'] = $form_state->getValue(['sendgrid', 'custom_fields']);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // Only for new submissions.
    if ($update) {
      return;
    }

    $configuration = $this->tokenManager->replace($this->configuration, $webform_submission);

    $listId = $configuration['list'];
    $emailField = $configuration['email_field'];
    $email = $webform_submission->getData()[$emailField];
    $fields = $configuration['reserved_fields'];
    $customFields = $configuration['custom_fields'];
    $this->addContact(
      $listId,
      $email,
      $fields,
      $customFields,
    );
  }

  /**
   * Get lists.
   *
   * @return array
   *   An array of arrays containing:
   *   - id: (string) an UUIDish ID generated by SendGrid.
   *   - name: (string) a label set by a SendGrid user.
   */
  protected function getLists(): array {
    // @see https://sendgrid.api-docs.io/v3.0/lists/get-all-lists
    $response = $this->sendGrid->client->marketing()->lists()->get();

    $json = $response->body();
    $decoded = Json::decode($json);

    return array_map(function (array $result): array {
      return array_intersect_key($result, array_flip(['id', 'name']));
    }, $decoded['result']);
  }

  /**
   * Get reserved and custom field definitions.
   *
   * @return array
   *   An array of arrays containing:
   *   Keys 'reserved' and 'custom', containing arrays of:
   *   - id: (string) an ID generated by SendGrid.
   *   - name: (string) a label set by a SendGrid user.
   *   - field_type: (string) the type of data.
   */
  protected function getFieldDefinitions(): array {
    // @see https://sendgrid.api-docs.io/v3.0/custom-fields/get-all-field-definitions
    $response = $this->sendGrid->client->marketing()->field_definitions()->get();

    $json = $response->body();
    $decoded = Json::decode($json);

    $reservedFields = array_map(function (array $result): array {
      return array_intersect_key($result, array_flip([
        'id',
        'name',
        'field_type',
      ]));
    }, $decoded['reserved_fields'] ?? []);

    $customFields = array_map(function (array $result): array {
      return array_intersect_key($result, array_flip([
        'id',
        'name',
        'field_type',
      ]));
    }, $decoded['custom_fields'] ?? []);

    return ['reserved' => $reservedFields, 'custom' => $customFields];
  }

  /**
   * Adds a contact to a list.
   *
   * @param string $listId
   *   The list ID. Looks like a GUID.
   * @param string $email
   *   The email address.
   * @param array $reservedFields
   *   Optional reserved fields keyed by name. Current valid fields include:
   *   - address_line_1 (string)
   *   - address_line_2 (string)
   *   - alternate_emails (array)
   *   - city (string)
   *   - country (string)
   *   - first_name (string)
   *   - last_name (string)
   *   - postal_code (string)
   *   - state_province_region (string)
   * @param array $customFields
   *   Optional reserved fields keyed by ID. ID is not the name set by a user,
   *   but rather generated by SendGrid. See getFieldDefinitions.
   *
   * @return bool
   *   Whether the contact was added to the list.
   */
  protected function addContact(string $listId, string $email, array $reservedFields = [], array $customFields = []): bool {
    // Email is a required field already set by $email parameter.
    unset($reservedFields['email']);

    $requestBody = new \stdClass();
    $requestBody->list_ids = [$listId];
    $contact = new \stdClass();
    // Reserved fields.
    $contact->email = $email;
    foreach ($reservedFields as $fieldName => $value) {
      $contact->{$fieldName} = $value;
    }
    // Custom fields.
    $contact->custom_fields = new \stdClass();
    foreach ($customFields as $fieldName => $value) {
      $contact->custom_fields->{$fieldName} = $value;
    }
    $requestBody->contacts = [$contact];

    // @see https://sendgrid.api-docs.io/v3.0/contacts/add-or-update-a-contact
    $response = $this->sendGrid->client->marketing()->contacts()->put($requestBody);

    return $response->statusCode() === 202;
  }

}
